#                                 **       FRAME iM8**

![Frame iM8](Frame_iM8_25mm.png))

Proyecto basado en este otro https://www.youtube.com/watch?v=lF6kriqJKbI de JC_3DESING, que ha su vez esta basado en 
este otro https://www.thingiverse.com/thing:2912974 de BILLPEALER, aunque se ha hecho desde cero con FreeCad y no usa 
ninguna de sus piezas originales.
El proyecto esta pensado inicialmente para una Anet A8 pero se puede extrapolar acuanquier impresora tipo cartesiana.
Con respecto a la anet A8 hay que comprar dos varillas lisas de 400mm (no lo he probado todavia) segun el proyecto original para el eje Y.

Hay mejoras sustanciales con respecto al proyecto original:
1.  desfase de 1mm en el eje Z.
2.  mejor apriete de los tornillos y tuercas (hare video al respecto).
3.  diseñado para reducir mucho mas la necesidad de soportes.
4.  posibilidad de aumentar los tamaños de impresion a un bajo costo.
  
Consejos de impresion:

1.  relleno al 50% o mas
2.  5 perimetros exteriores y tambien arriba y abajo
3.  altura de capa 2mm
4.  temperatura 220 ºC (esa es mi opinión)





